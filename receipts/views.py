from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from receipts.forms import ReceiptForm, CategoryForm, AcccountForm
from django.contrib.auth.decorators import login_required

@login_required
def receipt_view(request):
    receipt_view = Receipt.objects.filter(purchaser=request.user)

    context = {
        "receipt_view": receipt_view,
    }
    return render(request, "receipts/list.html", context)

@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()

        context = {
            "form": form,
        }

        return render(request, "receipts/create.html", context)

@login_required
def create_category(request):
    if request.method == "POST":
        form = CategoryForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.owner = request.user
            receipt.save()
            return redirect("category_list")
    else:
        form = CategoryForm()

        context = {
            "form": form,
        }

        return render(request, "receipts/create_category.html", context)


@login_required
def category_list(request):
    category_list = ExpenseCategory.objects.filter(owner=request.user)

    context = {
        "category_list": category_list,
    }
    return render(request, "receipts/category_list.html", context)

@login_required
def account_list(request):
    account_list = Account.objects.filter(owner=request.user)

    context = {
        "account_list": account_list,
    }
    return render(request, "receipts/account_list.html", context)

@login_required
def create_account(request):
    if request.method == "POST":
        form = AcccountForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.owner = request.user
            receipt.save()
            return redirect("account_list")
    else:
        form = AcccountForm()

        context = {
            "form": form,
        }

        return render(request, "receipts/create_account.html", context)
